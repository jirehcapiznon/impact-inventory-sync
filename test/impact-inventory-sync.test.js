/* global describe, it, after, before */
'use strict'

// const should = require('should')
const amqp = require('amqplib')

let _app = null
let _conn = null
let _channel = null


describe('Impact Inventory Sync', () => {
  process.env.PLUGIN_ID = 'demo.dev-sync'
  process.env.BROKER = 'amqp://guest:guest@127.0.0.1/'
  process.env.LOGGERS = 'logger1, logger2'
  process.env.CONFIG = '{"username": "AiriyanH", "password": "Reekoh#2018"}'

  let BROKER = process.env.BROKER
  let PLUGIN_ID = process.env.PLUGIN_ID

  before('init', function () {
    amqp.connect(BROKER).then((conn) => {
      _conn = conn
      return conn.createChannel()
    }).then((channel) => {
      _channel = channel
    }).catch((err) => {
      console.log(err)
    })
  })

  after('terminate', function () {
    _conn.close()
  })

  describe('#start', function () {
    it('should start the app', function (done) {
      this.timeout(10000)
      _app = require('../app')
      _app.once('init', done)
    })
  })

  describe('#sync', function () {
    it('should execute device sync', function (done) {
      this.timeout(8000)
      _channel.sendToQueue(PLUGIN_ID, Buffer.from(JSON.stringify({ operation: 'sync' })))
      _app.on('syncDone', done)
    })
  })

  // describe('#adddevice', () => {
  //   it('should add the device', function (done) {
  //     this.timeout(10000)

  //     _channel.sendToQueue(PLUGIN_ID, Buffer.from(JSON.stringify({
  //       operation: 'adddevice',
  //       device: {
  //         deviceId: '0BB2016050500111',
  //         manufacturer: 'Generic',
  //         model: 'Sensor'
  //       }
  //     })))

  //     _app.on('adddevice.ok', done)
  //   })
  // })

  // describe('#updatedevice', function () {
  //   it('should update the device', function (done) {
  //     this.timeout(10000)

  //     // TODO: send 'updatedevice' operation to reekoh queue

  //     /**
  //      * Example:
  //      *
  //      * _channel.sendToQueue(PLUGIN_ID, new Buffer(JSON.stringify({
  //      *   operation: 'updatedevice',
  //      *   device: {
  //      *     _id: 123, name: 'device123'
  //      *   }
  //      * })))
  //      *
  //      * _app.on('update.ok', done)
  //      *
  //      */
  //   })
  // })

  // describe('#removedevice', function () {
  //   it('should remove the device', function (done) {
  //     this.timeout(10000)

  //     _channel.sendToQueue(PLUGIN_ID, Buffer.from(JSON.stringify({
  //       operation: 'removedevice',
  //       device: {
  //         id: '2042'
  //       }
  //     })))

  //     _app.on('removedevice.ok', done)

  //   })
  // })
})

