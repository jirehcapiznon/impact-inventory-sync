'use strict'

const reekoh = require('reekoh')
const plugin = new reekoh.plugins.InventorySync()
/* eslint-disable new-cap */
const rkhLogger = new reekoh.logger('impact-inventory-sync')

let request = require('request-promise')
let BPromise = require('bluebird')
let get = require('lodash.get')

// let handleErr = (err) => {
//   console.log(`Error: ${err.message}`)
//   plugin.logException(err)
// }

plugin.on('sync', () => {
  let options = plugin.config

  return request.get({
    uri: 'https://dm.iot.nokia.com/rest/device',
    header: {
      ContentType: 'application/json'
    },
    auth: {
      username: options.username,
      password: options.password
    },
    json: true
  })
    .then((body) => {
      if (body.error) return BPromise.reject(new Error(body.error.message || body.error))
      else {
        let devices = get(body, 'aaData')

        BPromise.each(devices, device => {
          let deviceInfo = {
            name: `Impact Device - ${device.deviceId}`,
            _id: JSON.stringify(device.id),
            metadata: Object.assign({}, device)
          }
          console.log(deviceInfo)

          return plugin.syncDevice(deviceInfo)
            .then(() => {
              plugin.log({
                title: 'Impact Inventory Sync - Synced',
                device: deviceInfo
              })
            }).catch(plugin.logException)
        })
        plugin.emit('syncDone')
      }
    }).catch(plugin.logException)
})

// ADD DEVICE TO NOKIA
// plugin.on('adddevice', (device) => {
//   let option = plugin.config

//   return request({
//     method: 'POST',
//     uri: 'https://dm.iot.nokia.com/rest/device',
//     header: {
//       'content-type': 'application/json'
//     },
//     json: true,
//     auth: {
//       username: option.username,
//       password: option.password
//     },
//     body: {
//       'deviceId': device.deviceId,
//       'manufacturer': device.manufacturer,
//       'model': device.model
//     }
//   })
//     .then((err) => {
//       if (err) {
//         handleErr(err)
//       } else {
//         console.log('adddevice.ok')
//         plugin.emit('adddevice.ok')
//       }
//     })
//     .catch(plugin.logException)
// })

// DELETE DEVICE FROM NOKIA
// plugin.on('removedevice', (device) => {
//   let option = plugin.config

//   return request({
//     method: 'DELETE',
//     uri: `https://dm.iot.nokia.com/rest/device/${device.id}`,
//     header: {
//       Accept: 'application/json'
//     },
//     auth: {
//       username: option.username,
//       password: option.password
//     }
//   }).then((err) => {
//     if (err) {
//       handleErr(err)
//     } else {
//       console.log('removedevice.ok')
//       plugin.emit('removedevice.ok')
//     }
//   }).catch(plugin.logException)
// })

plugin.once('ready', () => {
  rkhLogger.info('Impact Inventory Sync has been initialized.')
  plugin.log('Impact Inventory Sync has been initialized.')
  plugin.emit('init')
})

module.exports = plugin
