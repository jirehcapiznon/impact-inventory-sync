# Impact Inventory Sync

## Description

Impact Inventory Sync plugin enables integration with the Nokia Device Management API to sync devices between Nokia Impact and the Reekoh Device Registry.


## Configuration

To configure the plugin, you will be asked to provide the following details:

- **Inventory Sync Name** - This is a label given to your plugin to locate it easily.
- **Username** - Nokia Username.
- **Password** - Nokia Password.

![](https://reekohdocs.blob.core.windows.net/plugin-docs/inventory-sync/impact-inventory-sync/1.0.0/impack-inverntory-sync-config.png) 

## Sync Devices

Syncing of your Nokia Impact Devices will be executed depending on how frequent you set it in the configuration. But, you can manually sync your devices by going to Devices -> Inventory Sync.

![](https://reekohdocs.blob.core.windows.net/plugin-docs/inventory-sync/impact-inventory-sync/1.0.0/impact-inventory-sync-dash.png)

Click the Sync button on the right side of the Inventory Sync plugin that you configured.

![](https://reekohdocs.blob.core.windows.net/plugin-docs/inventory-sync/impact-inventory-sync/1.0.0/impack-inverntory-sync-sync.png)

## Verification

To verify if syncing is successful, go to Reekoh's Device Management section and compare the devices registered in Reekoh against the devices registered in Nokia Impact Backend.

![](https://reekohdocs.blob.core.windows.net/plugin-docs/inventory-sync/impact-inventory-sync/1.0.0/impack-inverntory-sync-nokia.png)

![](https://reekohdocs.blob.core.windows.net/plugin-docs/inventory-sync/impact-inventory-sync/1.0.0/impack-inverntory-sync-reekoh.png)