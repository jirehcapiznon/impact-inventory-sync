FROM node:boron

MAINTAINER Reekoh

COPY . /home/node/impact-inventory-sync

WORKDIR /home/node/impact-inventory-sync

# Install dependencies
# RUN npm install pm2@2.6.1 -g

# CMD pm2-docker --json app.yml
CMD ["node", "app.js"]